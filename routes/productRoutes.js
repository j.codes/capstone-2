const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')

// Create new product
router.post("/create", auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.addProduct(data).then((result) => { 
		response.send(result)
	})
})

// Get all ACTIVE products
router.get("/active", auth.verify, (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get all understocked products
router.get("/understock", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.getAllUnderstocked(data).then((result) => {
		response.send(result)
	})
})

// Get all INACTIVE products
router.get("/inactive", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.getAllInActive(data).then((result) => {
		response.send(result)
	})
})

// Get single product
router.get("/:productId", auth.verify, (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

// Update single product
router.patch("/:productId/update", auth.verify, (request, response) => {
	const data = {
		product: request.body,
		productId: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.updateProduct(data).then((result) => {
		response.send(result)
	})
})

// Archive a product
router.patch("/:productId/archive", auth.verify, (request, response) => {
	const data = {
		productId: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.archiveProduct(data).then((result) => {
		response.send(result)
	})
})

module.exports = router