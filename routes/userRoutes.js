const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// Register new user
router.post("/register", (request, response) => {
    UserController.register(request.body).then((result) => {
        response.send(result)
    })
})

// Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Get user details
router.get("/:userId", auth.verify, (request, response) => {
	const data = {
		userId: request.params.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.getUserDetails(data).then((result) => {
		response.send(result)
	})
})

// Set user as admin
router.patch("/:userId/admin", auth.verify, (request, response) => {
	const data = {
		userId: request.params.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.setUserAsAdmin(data).then((result) => {
		response.send(result)
	})
})

module.exports = router