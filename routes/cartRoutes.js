const express = require('express')
const router = express.Router()
const CartController = require('../controllers/CartController')
const auth = require('../auth')

// Add product/s to CART
router.post("/add", auth.verify, (request, response) => {
	let data = {
		product: request.body,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	CartController.addToCart(data).then((result) => {
		response.send(result)
	})
})

// Change product quantity / delete product from cart
router.patch("/:orderId/edit", auth.verify, (request, response) => {
	let data = {
		product: request.body,
		orderId: request.params.orderId,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	CartController.editCart(data).then((result) => {
		response.send(result)
	})
})

module.exports = router