const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')

// Order a product
router.post("/create", auth.verify, (request, response) => {
	let data = {
		product: request.body,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.orderProduct(data).then((result) => {
		response.send(result)
	})
})

// Get user order
router.get("/:orderId/details", auth.verify, (request, response) => {
	const data = {
		orderId: request.params.orderId,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getUserOrderDetails(data).then((result) => {
		response.send(result)
	})
})

// Get user order
router.get("/:orderId/details", auth.verify, (request, response) => {
	const data = {
		orderId: request.params.orderId,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getUserOrderDetails(data).then((result) => {
		response.send(result)
	})
})

// Get all user orders
router.get("/history", auth.verify, (request, response) => {
	const data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getUserOrderHistory(data).then((result) => {
		response.send(result)
	})
})

// Get all orders (admin)
router.get("/all", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getAllOrders(data).then((result) => {
		response.send(result)
	})
})

// Get all pending orders (admin)
router.get("/pending", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getAllPendingOrders(data).then((result) => {
		response.send(result)
	})
})

// Get all confirmed orders (admin)
router.get("/confirmed", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getAllConfirmedOrders(data).then((result) => {
		response.send(result)
	})
})

// Order checkout
router.patch("/:orderId/checkout", auth.verify, (request, response) => {
	const data = {
		order: request.body,
		orderId: request.params.orderId,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.orderCheckout(data).then((result) => {
		response.send(result)
	})
})

// Request to cancel order
router.patch("/:orderId/cancel", auth.verify, (request, response) => {
	const data = {
		order: request.body,
		orderId: request.params.orderId,
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.requestOrderCancellation(data).then((result) => {
		response.send(result)
	})
})

// Get all pending orders requested for cancellation (admin)
router.get("/cancel/requests", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getAllPendingCancelledOrders(data).then((result) => {
		response.send(result)
	})
})

// Cancell order
router.patch("/:orderId/cancel/approve", auth.verify, (request, response) => {
	const data = {
		orderId: request.params.orderId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.approveOrderCancellation(data).then((result) => {
		response.send(result)
	})
})

// Get all cancelled orders (admin)
router.get("/cancelled", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getAllCancelledOrders(data).then((result) => {
		response.send(result)
	})
})

// Set order as completed
router.patch("/:orderId/completed", auth.verify, (request, response) => {
	const data = {
		orderId: request.params.orderId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.setOrderAsCompleted(data).then((result) => {
		response.send(result)
	})
})

// Get all completed orders (admin)
router.get("/completed", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.getAllCompletedOrders(data).then((result) => {
		response.send(result)
	})
})

module.exports = router