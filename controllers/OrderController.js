const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Order a product
module.exports.orderProduct = async (data) => {

	if (data.isAdmin) {
		let message = Promise.resolve({
			message: "An ADMIN user is not allowed to access this!"
		})

		return message.then((value) => {
			return value
		})
	}

	let totalAmount
	let pendingCount
	let availableStocks

	await Product.findById(data.product.productId).then((result) => {
	 	totalAmount = result.price * data.product.quantity
	 	availableStocks = result.stocks - data.product.quantity

	 	result.stocks = availableStocks

	 	if(availableStocks == 0) {
	 		result.isActive = false
	 	}

		return result.save().then((added_order, error) => {
			if(error){
				return false
			}

			return true
		})
	})

	await Order.find({userId: data.userId, orderStatus: "Pending"}).then((result) => {
		pendingCount = result.length
	})

	await Order.findOne({userId: data.userId}).then((result) => {
		if(result == null || pendingCount == 0){
			let new_order = new Order ({
				userId: data.userId,
				totalAmount: 0
			})

			return new_order.save().then((new_order, error) => {
				if(error){
					return false
				}
					return true
			})
		}
	})

	await Order.findOne({userId: data.userId, orderStatus: "Pending"}).then((result) => {
		result.products.push({
			productId: data.product.productId,
			quantity: data.product.quantity,
			subTotal: totalAmount
		})

		result.totalAmount += totalAmount

		return result.save().then((added_order, error) => {
			if(error){
				return false
			}

			return true
		})
	})
	
	return {
		message: "Product successfully ordered."
	}
}

// Get user order
module.exports.getUserOrderDetails = (data) => {
	if (data.isAdmin) {
		let message = Promise.resolve({
			message: "An ADMIN user is not allowed to access this!"
		})

		return message.then((value) => {
			return value
		})
	}

	return Order.find({_id: data.orderId, userId: data.userId}).then((result) => {
		if(result.length > 0)
		{
			return result
		}

		let message = Promise.resolve({
			message: "Access denied!"
		})

		return message.then((value) => {
			return value
		})
		
	})
}

// Get all user orders
module.exports.getUserOrderHistory = (data) => {
	if (data.isAdmin) {
		let message = Promise.resolve({
			message: "An ADMIN user is not allowed to access this!"
		})

		return message.then((value) => {
			return value
		})
	}

	return Order.find({userId: data.userId}).then((result) => {
		if(result.length > 0)
		{
			return result
		}

		let message = Promise.resolve({
			message: "Access denied!"
		})

		return message.then((value) => {
			return value
		})
		
	})
}

// Get all orders (admin)
module.exports.getAllOrders = (data) => {
	if (data.isAdmin) {
		return Order.find({}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Get all pending orders (admin)
module.exports.getAllPendingOrders = (data) => {
	if (data.isAdmin) {
		return Order.find({orderStatus: "Pending"}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Get all confirmed orders (admin)
module.exports.getAllConfirmedOrders = (data) => {
	if (data.isAdmin) {
		return Order.find({orderStatus: "Confirmed"}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Order checkout
module.exports.orderCheckout = (data) => {
	if (data.isAdmin) {
		let message = Promise.resolve({
			message: "An ADMIN user is not allowed to access this module!"
		})

		return message.then((value) => {
			return value
		})
	}

	return Order.findByIdAndUpdate(data.orderId, {
		orderStatus: "Confirmed", 
		MOD: data.order.MOD, 
		deliveryAddress: data.order.deliveryAddress, 
		MOP: data.order.MOP
	}).then((updated_order, error) => {
		if(error){
			return false
		}

		return {
			message: "The order has been successfully checked out!"
		}
	})
}

// Request to cancel order
module.exports.requestOrderCancellation = async (data) => {
	let isEligible = false

	if (data.isAdmin) {
		let message = Promise.resolve({
			message: "An ADMIN user is not allowed to access this!"
		})

		return message.then((value) => {
			return value
		})
	}

	await Order.findById(data.orderId).then((result) => {
		if(result.orderStatus == "Pending"){
			isEligible = true
		}

		if(result.cancelReason != null){
			isEligible = false
		}
	})

	if(isEligible && data.order.reason !== "" && data.order.reason !== null)
	{
		return Order.findByIdAndUpdate(data.orderId, {
			cancelReason: data.order.reason
		}).then((updated_order, error) => {
			if(error){
				return false
			}

			return {
				message: "Request for cancellation of this order has been submitted."
			}
		})
	} else {
		let message = Promise.resolve({
			message: "Invalid request!"
		})

		return message.then((value) => {
			return value
		})
	}
}

// Get all pending orders requested for cancellation (admin)
module.exports.getAllPendingCancelledOrders = (data) => {
	if (data.isAdmin) {
		return Order.find({orderStatus: "Pending", cancelReason: {$ne:null}}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Cancell order
module.exports.approveOrderCancellation = async (data) => {
	let cancelledProducts

	if (data.isAdmin) {
		await Order.findById(data.orderId).then((result) => {

			cancelledProducts = result.products
		})

		await Order.findByIdAndUpdate(data.orderId, {
			orderStatus: "Cancelled"
		}).then((updated_order, error) => {
			if(error){
				return false
			}

			return true
		})

		for (let i = 0; i < cancelledProducts.length; i++ )
		{
			await Product.findById(cancelledProducts[i].productId).then((result) => {
			 	result.stocks += cancelledProducts[i].quantity

				return result.save().then((added_order, error) => {
					if(error){
						return false
					}

					return true
				})
			})
		}
	} else {
		let message = Promise.resolve({
			message: "User must be ADMIN to access this."
		})

		return message.then((value) => {
			return value
		})
	}

	let message = Promise.resolve({
		message: "Order successfully tagged as cancelled."
	})

	return message.then((value) => {
		return value
	})
}

// Get all cancelled orders (admin)
module.exports.getAllCancelledOrders = (data) => {
	if (data.isAdmin) {
		return Order.find({orderStatus: "Cancelled"}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Set order as completed
module.exports.setOrderAsCompleted = (data) => {
	if (data.isAdmin) {
		return Order.findByIdAndUpdate(data.orderId, {
			orderStatus: "Completed"
		}).then((updated_order, error) => {
			if(error){
				return false
			}

			return {
				message: "Order has been set as Completed."
			}
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Get all completed orders (admin)
module.exports.getAllCompletedOrders = (data) => {
	if (data.isAdmin) {
		return Order.find({orderStatus: "Completed"}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}