const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Add product/s to CART
module.exports.addToCart = async (data) => {

	if (data.isAdmin) {
		let message = Promise.resolve({
			message: "An ADMIN user is not allowed to access this!"
		})

		return message.then((value) => {
			return value
		})
	}

	let totalAmount
	let pendingCount
	let availableStocks

	await Product.findById(data.product.productId).then((result) => {
	 	totalAmount = result.price * data.product.quantity
 	 	availableStocks = result.stocks - data.product.quantity

 	 	result.stocks = availableStocks

 		return result.save().then((added_order, error) => {
 			if(error){
 				return false
 			}

 			return true
 		})
	})

	await Order.find({userId: data.userId, orderStatus: "Pending"}).then((result) => {
		pendingCount = result.length
	})

	await Order.findOne({userId: data.userId}).then((result) => {
		if(result == null || pendingCount == 0){
			let new_order = new Order ({
				userId: data.userId,
				totalAmount: 0
			})

			return new_order.save().then((new_order, error) => {
				if(error){
					return false
				}
					return true
			})
		}
	})

	await Order.findOne({userId: data.userId, orderStatus: "Pending"}).then((result) => {
		result.products.push({
			productId: data.product.productId,
			quantity: data.product.quantity,
			subTotal: totalAmount
		})

		result.totalAmount += totalAmount

		return result.save().then((added_order, error) => {
			if(error){
				return false
			}

			return true
		})
	})
	
	return {
		message: "Product successfully added to cart."
	}
}

// Change product quantity / delete product from cart
module.exports.editCart = async (data) => {
	let newTotalAmount
	let newSubTotal
	let oldTotalAmount
	let oldSubTotal
	let availableStocks

	if(data.product.quantity > 0){
		await Product.findById(data.product.productId).then((result) => {
		 	newSubTotal = result.price * data.product.quantity
		})

		await Order.findById(data.orderId).then((result) => {

			var index = result.products.findIndex(item => item.productId === data.product.productId);

			oldTotalAmount = result.totalAmount
			oldSubTotal = result.products[index].subTotal
			availableStocks = result.products[index].quantity - data.product.quantity

			newTotalAmount = (oldTotalAmount - oldSubTotal) + newSubTotal
		})

		await Order.findByIdAndUpdate(data.orderId, {
				totalAmount: newTotalAmount
			}).then((updated_cart, error) => {
			if(error){
				return false
			}

			return true
		})

		await Order.updateOne({_id: data.orderId, "products.productId": data.product.productId},{$set: {"products.$.quantity": data.product.quantity, "products.$.subTotal": newSubTotal}}
		).then((updated_cart, error) => {
			if(error){
				return false
			}

			return updated_cart
		})

		await Product.findById(data.product.productId).then((result) => {
	 	 	if(result.stocks == (-1 * availableStocks)) {
	 	 		result.isActive = false
	 	 	}

	 	 	if(result.stocks == 0 && availableStocks > 0) {
	 	 		result.isActive = true
	 	 	}

	 	 	result.stocks += availableStocks

	 		return result.save().then((added_order, error) => {
	 			if(error){
	 				return false
	 			}

	 			return true
	 		})
		})

		let message = Promise.resolve({
			message: "Cart successfully updated!"
		})

		return message.then((value) => {
			return value
		})
	}

	if (data.product.quantity == 0)
	{
		await Order.findById(data.orderId).then((result) => {

			var index = result.products.findIndex(item => item.productId === data.product.productId);

			oldTotalAmount = result.totalAmount
			oldSubTotal = result.products[index].subTotal
			availableStocks = result.products[index].quantity

			newTotalAmount = oldTotalAmount - oldSubTotal
		})

		await Order.findByIdAndUpdate(data.orderId, {
				totalAmount: newTotalAmount
			}).then((updated_cart, error) => {
			if(error){
				return false
			}

			return true
		})

		await Order.update({orderId: data.orderId}, {$pull: {products: {productId : data.product.productId}}}).then((updated_cart, error) => {
			if(error){
				return false
			}

			return true
		})

		await Product.findById(data.product.productId).then((result) => {
	 	 	if(result.isActive == false) {
	 	 		result.isActive = true
	 	 	}

	 	 	result.stocks += availableStocks

	 		return result.save().then((added_order, error) => {
	 			if(error){
	 				return false
	 			}

	 			return true
	 		})
		})

		let message = Promise.resolve({
			message: "Item/s successfully removed from cart!"
		})

		return message.then((value) => {
			return value
		})
	}

	let message = Promise.resolve({
		message: "Invalid input!"
	})

	return message.then((value) => {
		return value
	})
}