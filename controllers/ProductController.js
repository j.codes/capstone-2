const Product = require('../models/Product')

// Create new product
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let new_product = new Product ({
			productName: data.product.name,
			description: data.product.description,
			price: data.product.price,
			stocks: data.product.stocks
		})

		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}

			return {
				message: "New product successfully created!"
			}
		})
	} 

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Get all ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}

// Get all INACTIVE products
module.exports.getAllInActive = (data) => {
	if (data.isAdmin) {
		return Product.find({isActive: false}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Get all understocked products
module.exports.getAllUnderstocked = (data) => {
	if (data.isAdmin) {
		return Product.find({stocks: { $lte: 3 }}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Get single product
module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result
	})
}

// Update single product
module.exports.updateProduct = (data) => {
	if (data.isAdmin) {
		return Product.findByIdAndUpdate(data.productId, {
			productName: data.product.name,
			description: data.product.description,
			price: data.product.price,
			stocks: data.product.stocks,
			isActive: data.product.isActive
		}).then((updated_product, error) => {
			if(error){
				return false
			}

			return {
				message: "product has been updated successfully!"
			}
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Archive a product
module.exports.archiveProduct = (data) => {
	if (data.isAdmin) {
		return Product.findByIdAndUpdate(data.productId, {
			isActive: false
		}).then((archived_product, error) => {
			if(error){
				return false
			}

			return {
				message: "The product has been archived successfully!"
			}
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}