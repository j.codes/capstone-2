const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Register new user
module.exports.register = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		// Checks if the email is not duplicate
		if(result != null){
			return {
				message: "Email already exists!"
			}
		}

		var encrypted_password = bcrypt.hashSync(data.password, 10)

		let new_user = new User({
			firstName: data.firstName,
			lastName: data.lastName,
			email: data.email,
			mobileNo: data.mobileNo,
			password: encrypted_password
		})

		return new_user.save().then((created_user, error) => {
			if(error){
				return false
			}

			return {
				message: 'User successfully registered!'
			}
		})
	})
}

// Login user
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User does not exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password) 

		if(is_password_correct){
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: "Password is incorrect!"
		}
	})
}

// Get user details
module.exports.getUserDetails = (data) => {
	if (data.isAdmin) {
		return User.findById(data.userId, {password: 0}).then((result) => {
			return result
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}

// Set user as admin
module.exports.setUserAsAdmin = (data) => {
	if (data.isAdmin) {
		return User.findByIdAndUpdate(data.userId, {
			isAdmin: true
		}).then((updated_user, error) => {
			if(error){
				return false
			}

			return {
				message: "The user has been successfully set as admin!"
			}
		})
	}

	let message = Promise.resolve({
		message: "User must be ADMIN to access this."
	})

	return message.then((value) => {
		return value
	})
}