const mongoose = require('mongoose')

const products_schema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, 'Product name is required.']
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	stocks: {
		type: Number,
		required: [true, 'Stocks is required.']
	}
})

module.exports = mongoose.model('Product', products_schema)