const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'UserID is required.']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is required.']
			},
			subTotal: {
				type: Number,
				required: [true, 'Subtotal amount is required.']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Total amount is required.']
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	orderStatus: {
		type: String,
		default: "Pending"
	},
	cancelReason: String,
	MOD: String,
	deliveryAddress: String,
	MOP: String
})

module.exports = mongoose.model('Order', order_schema)