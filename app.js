const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')
const cartRoutes = require('./routes/cartRoutes')

dotenv.config()

const app = express()
const port = process.env.PORT || 8080
//const port = 8080

// MongoDB Connection
mongoose.connect(`mongodb+srv://jcodesx:${process.env.MONGODB_PASSWORD}@cluster0.hiowweg.mongodb.net/e-commerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongoDB!'))
// MongoDB Connection END

// To avoid CORS errors when trying to send request to our server
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/order', orderRoutes)
app.use('/cart', cartRoutes)
// Routes END

app.listen(port, () => {
	console.log(`API is now running on localhost:${port}`)
})